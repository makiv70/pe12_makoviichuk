/**
 *      Переменные, объявленные с помощью var & let могут менять свои значения, их можно переопределять на
 * протяжение выполнения кодаю. Переменная, объявленная с помощью const своего значения уже не меняет.
 *
 * ***********
 *      Разница между var & let состоит в том, что переменная, объявленная через var будет иметь область
 *  видимости как внутри блока, так и за его пределами, что не всегда удобно. Тогда как переменная let имеет область
 *  видимости  только внутри блока, в котором была объявлена.
 *
 *  ************
 *      Именно по причине того, что var имеет область видимости вне блока, в которм была объявлена,  её применение
 *  считается плохим тоном.
 *
 * */

let userName = prompt('Как Вас зовут', ' ');

let userAge = prompt('Сколько Вам лет', 18);

if (userAge<18) {
    alert ('You are not allowed to visit this website.');

} else if(userAge >=18 && userAge<=22)
{ let result=confirm ('Are you sure you want to continue?')
    if(result==true) {alert ('Welcome,' + ' '+ userName)} else { alert('You are not allowed to visit this website')}}
else if(userAge >22) {alert('Welcome' + ' ' + userName)}


