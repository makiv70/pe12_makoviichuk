
/**
 * Метод- это свойство объекта, заданное через функцию.
 * Может показаться, что определение просто списано из материалов, но нет! Просто столько перечитал про объекты,
 * свойства и методы, что вот как то отложилось вот так вот.))
 *
 *
 * */
function createNewUser (){
    const firstName=prompt('А как Вас зовут','пример:Вася')
    const lastName=prompt('А фамилия','пример:Сталин')

    const newUser={
        firstname:firstName,
        lastName:lastName,
        getLogin:function () {
            return `${firstName[0].toLowerCase()}${lastName.toLowerCase()}`

        }
    }
    return newUser
}

const student = createNewUser();

console.log(student);

console.log(student.getLogin());


